<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');
Route::get('/logout', 'HomeController@logout');
Route::get('/userProfile', 'HomeController@userProfile');

Route::post('/login', 'HomeController@login');
Route::post('/register', 'HomeController@register');


Route::get('/timer/list', 'TimerController@timers');

Route::post('/timer/stop', 'TimerController@stop');
Route::post('/timer/start', 'TimerController@start');
Route::post('/timer/remove', 'TimerController@remove');
Route::post('/timer/add', 'TimerController@add');
