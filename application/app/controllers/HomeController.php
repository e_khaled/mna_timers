<?php

class HomeController extends BaseController {


    public function index()
    {
        return View::make('index');
    }

    public function login()
    {
        $email    = Input::get('username');
        $password = Input::get('password');

        Auth::attempt(array('username' => $email, 'password' => $password));

        return $this->userProfile();
    }

    public function logout()
    {
        Auth::logout();
        Response::json(array('status' => 'success'));
    }

    public function register()
    {
        $input = Input::all();
        $v_rules = array(
            'email'       => 'required|email|unique:user,username',
            'first_name'  => 'required',
            'last_name'   => 'required',
            'password'    => 'required|confirmed'
        );

        $validator = Validator::make($input, $v_rules);

        if($validator->fails()){
            //if validation fails, make sure to let the frontend know
            return Response::json($validator->messages(), 500);
        }

        $user = new User;
        $user->username   = $input['email'];
        $user->first_name = $input['first_name'];
        $user->last_name  = $input['last_name'];
        $user->password   = Hash::make($input['password']);

        $user->save();

        //log the user in
        Auth::loginUsingId($user->id);

        return Response::json($user);
    }

    /**
     * Method used by the frontend frequently to check if user is logged in or not
     *
     * @author Khaled Ahmed
     */
    public function userProfile()
    {
        if(Auth::check()){
            return Response::json(Auth::user());
        }else{
            App::abort('403', 'Access Denied');
        }
    }

}
