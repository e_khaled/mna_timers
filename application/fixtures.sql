CREATE TABLE `user` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(255) NOT NULL,
  `password` VARCHAR(60) NOT NULL,
  `first_name` VARCHAR(255) NULL,
  `last_name` VARCHAR(255) NULL,
  `remember_token` VARCHAR(100) NULL,
  `created_at` DATETIME NULL,
  `updated_at` DATETIME NULL,
  PRIMARY KEY (`id`));


CREATE TABLE `timer` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NULL,
  `identifier` VARCHAR(60) NULL,
  `user_id` INT(11) NOT NULL,
  `state` TINYINT(1) NOT NULL DEFAULT 0,
  `created_at` DATETIME NULL,
  `updated_at` DATETIME NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_timer_user_idx` (`user_id` ASC),
  CONSTRAINT `fk_timer_user`
    FOREIGN KEY (`user_id`)
    REFERENCES `user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

CREATE TABLE `timer_activity` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `timer_id` INT(11) NOT NULL,
  `stopped_at` DATETIME NULL,
  `created_at` DATETIME NULL,
  `updated_at` DATETIME NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_timer_timer_activity_idx` (`timer_id` ASC),
  CONSTRAINT `fk_timer_timer_activity`
    FOREIGN KEY (`timer_id`)
    REFERENCES `timer` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);