Sablier.controller('timersCtrl', function($scope, Auth, $location, $http, $interval){

  $scope.timers = [];
  $scope.loading = 1;
  $scope.timer = {};

  //capture `removed` event coming from child controllers
  //and remove the scope item
  //because child controllers cannot remove themselves
  $scope.$on('timer:removed', function(ev, idx){
    $scope.timers.splice(idx, 1);
  });

  $scope.add = function(ev){
    if(ev){
      var el = angular.element(ev.target),
      button = el.find('button[type=submit]'),
      curHtml = button.html();

      button.html('<i class="fa fa-spinner fa-spin"></i> Adding...').attr('disabled', true);

      $http.post(BASEURL + '/timer/add', $scope.timer)
      .success(function(timer){
        if(timer){
          $scope.timers.unshift(timer);
          el.parents('.modal').modal('hide');
        }else{
          alert("Timer could not be added due to an internal error");
        }
        $scope.timer = {};
      })
      .error(function(resp, code){
        el.find('input[name=name]').focus();
        alert("Timer could not be added due to an internal error");
      })
      .finally(function(){
        button.html(curHtml).attr('disabled', false);
      });
    }
  };

  //single time ticker to be used by all timer tiles
  function startTick($scope){
    $interval(function(){
      $scope.$broadcast('timer:tick');
    }, 1000);
  }

  (function(){
    Auth.check().then(
      function(){
        $http.get(BASEURL + '/timer/list')
        .success(function(timers){
          $scope.loading = 0;
          $scope.timers = timers;
          startTick($scope);
        }).error(function(){

        });
      },
      function(){
        $location.path('/login');
    });
  })();

});