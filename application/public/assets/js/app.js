var Sablier = angular.module('SablierModule', ['ngAnimate', 'ngRoute', 'ngSanitize']);

Sablier.config(['$routeProvider', function($routeProvider){

  //angular routes
  var tpl_prefix = BASEURL + '/assets/views/';
  $routeProvider
  .when('/registration', {
    templateUrl: tpl_prefix + 'registration.html',
    controller: 'regCtrl'
  })
  .when('/login', {
    templateUrl: tpl_prefix + 'login.html',
    controller: 'loginCtrl'
  })
  .when('/timers', {
    templateUrl: tpl_prefix + 'timers.html',
    controller: 'timersCtrl'
  })
  .otherwise({redirectTo: '/login'})

}]);