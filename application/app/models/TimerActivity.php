<?php

class TimerActivity extends Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'timer_activity';

    public function timer()
    {
        return $this->belongsTo('Timer');
    }

}
