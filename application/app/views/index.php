<!doctype html>
<html lang="en" ng-app="SablierModule">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="Content-type" content="text/html; charset=utf-8">
        <title>Le
Sablier</title>

        <script type="text/javascript">
        var BASEURL = '<?=URL::to('/')?>';
        </script>

        <link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
        <link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css">

        <link rel="stylesheet" type="text/css" href="<?=asset('assets/css/animations.css')?>">
        <link rel="stylesheet" type="text/css" href="<?=asset('assets/css/app.css')?>">

    </head>
    <body>

        <div id="wrapper" class="container-fluid">
            <div id="page-content-wrapper">
                <div class="navbar navbar-inverse navbar-fixed-top">
                  <p class="navbar-text navbar-right pull-right" ng-controller="statusCtrl"><span ng-if="user">Welcome {{user.first_name}} | <button ng-click="logout()" class="btn btn-xs btn-default"><i class="fa fa-sign-out"></i> Logout</button></span><span ng-if="!user">Have an account? <a href="#/login" class="btn btn-default btn-xs"><i class="fa fa-sign-in"></i> Login</a></span></p>
                  <div class="navbar-header">
                    <a class="navbar-brand" href="/"><span><i class="fa fa-clock-o"></i> Le
Sablier</span></a>

                  </div>
                </div>

                <div class="page-content">
                    <div ng-view autoscroll="true"></div>
                </div>
            </div>
        </div>






        <script type="text/javascript" src="//code.jquery.com/jquery-1.10.2.min.js"></script>
        <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/underscore.js/1.6.0/underscore-min.js"></script>
        <script type="text/javascript" src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.20/angular.min.js"></script>
        <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.20/angular-route.min.js"></script>
        <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.20/angular-sanitize.min.js"></script>
        <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.20/angular-animate.min.js"></script>

        <script src="<?=asset('assets/js/app.js')?>"></script>
        <script src="<?=asset('assets/js/services/AuthService.js')?>"></script>
        <script src="<?=asset('assets/js/directives/passwordCheck.js')?>"></script>
        <script src="<?=asset('assets/js/controllers/statusCtrl.js')?>"></script>
        <script src="<?=asset('assets/js/controllers/loginCtrl.js')?>"></script>
        <script src="<?=asset('assets/js/controllers/regCtrl.js')?>"></script>
        <script src="<?=asset('assets/js/controllers/timersCtrl.js')?>"></script>
        <script src="<?=asset('assets/js/controllers/timerBlockCtrl.js')?>"></script>

    </body>
</html>
