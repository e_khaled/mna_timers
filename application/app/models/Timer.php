<?php

class Timer extends Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'timer';


    public function user()
    {
        return $this->belongsTo('User');
    }

    public function timerActivities()
    {
        return $this->hasMany('TimerActivity');
    }

}
