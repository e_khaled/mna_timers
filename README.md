# How to Run #

### Using Vagrant ###

1. Make sure you have Vagrant >= 1.6 installed, if it isn't, install from [http://vagrantup.com](http://vagrantup.com)
2. Clone this repository, and navigate to it via the command line
3. Run `vagrant up` -- This will take a while the first time this is run because it has to download the virtual machine image etc.
4. Go make yourself a cuppa.
5. Once the machine is up, the application is available at [http://192.168.13.37/public](http://192.168.13.37/public)

### Without using Vagrant ###

1. Download this repository
2. Place the contents of `application` folder in you web directory
3. Run `composer install` in that directory to install all dependencies
4. Create a new database in your MySQL server
5. Update `WEBFOLDER/app/config/database.php` with your database details
6. Seed your new database with the contents of `WEBFOLDER/fixtures.sql`
7. Make `WEBFOLDER/app/storage` and its subfolders writable
8. The application will be available at _http://[your\_webhost]/public_

**NOTE:** Application hasn't been tested in any other browser except Chrome