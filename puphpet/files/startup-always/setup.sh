#!/bin/bash
cd /var/www/default

#install composer deps
/usr/local/bin/composer install

#set writable folders
chmod -R 0777 ./app/storage

#ensure php-fpm is started
service php-fpm restart