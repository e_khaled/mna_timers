<?php
/**
 * Anatomy of a timer
 * ==================
 * A timer is defined by an entry in the timer table
 *
 * When a timer is started, a new entry is created in the timer_activity table
 * with the stopped_at column as NULL
 *
 * when a timer is stopped, the stopped_at column gets populated with the current DateTime
 *
 * The frontend code uses the entries in the timer_activity table to calculate elapsed time
 *
 * @author Khaled Ahmed
 */

class TimerController extends BaseController {

    /**
     * Private varialble to hold current logged in user
     *
     * @var Object
     */
    private $user;

    /**
     * Constructor method
     * stores the logged in user
     * or kicks them out if not logged in
     *
     * @author Khaled Ahmed
     */
    public function __construct()
    {
        if(Auth::check()){
            $this->user = Auth::user();
        }else{
            App::abort('403', 'Access Denied');
        }
    }

    /**
     * Provides a list of all timers created by this user
     *
     * @author Khaled Ahmed
     */
    public function timers()
    {
        $timers = Timer::with(array(
            'timerActivities' => function($query){
                $query->orderBy('id', 'asc');
            }
        ))
        ->orderBy('id', 'desc')
        ->where('user_id',$this->user->id)->get();

        return Response::json($timers);
    }

    /**
     * Stops a given timer
     *
     * @author Khaled Ahmed
     */
    public function stop()
    {
        $ts       = Input::get('ts');
        $id       = Input::get('id');
        $activity = null;

        $timer = Timer::with(array(
            'timerActivities' => function($query){
                $query->orderBy('id', 'asc');
            }
        ))
        ->find($id);

        if($timer && $timer->user_id == $this->user->id){

            //has to be inside a transaction as both tables need updating atomically
            DB::transaction(function() use (&$timer, &$activity){
                //populate the stopped_at field for the most recent entry in the timer_activity table
                $activity = $timer->timerActivities->last();
                if($activity->stopped_at == null){
                    $activity->stopped_at = date("Y-m-d H:i:s");
                    $activity->save();
                }

                //turn timer off
                $timer->state = 0;
                $timer->save();
            });

            return Response::json(array(
                'status'     => 'success',
                'state'      => $timer->state,
                'stopped_at' => $activity->stopped_at
            ));

        }else{
            App::abort('500', 'Invalid timer');
        }

    }

    /**
     * Starts a given timer
     *
     * @author Khaled Ahmed
     */
    public function start()
    {
        $id       = Input::get('id');
        $timer    = Timer::find($id);
        $activity = null;

        if($timer && $timer->user_id == $this->user->id){

            //has to be inside a transaction as both tables need updating atomically
            DB::transaction(function() use (&$timer, &$activity){
                //create a new timer_activity row
                $activity = new TimerActivity;
                $timer->timerActivities()->save($activity);

                //turn timer on
                $timer->state = 1;
                $timer->save();
            });

            if(!empty($activity) && $activity->id && $timer->state == 1){
                return Response::json(array(
                    'status'         => 'success',
                    'state'          => $timer->state,
                    'timer_activity' => $activity
                ));
            }else{
                return Response::json(array('status' => 'failure'));
            }

        }else{
            App::abort('500', 'Invalid timer');
        }
    }

    /**
     * Deletes a given timer
     *
     * @author Khaled Ahmed
     */
    public function remove()
    {
        $id    = Input::get('id');
        $timer = Timer::with('timerActivities')->find($id);

        if($timer && $timer->user_id == $this->user->id){

            $tracker  = 0;
            $toDelete = count($timer->timerActivities) + 1;

            //has to be inside a transaction as both tables need updating atomically
            DB::transaction(function() use (&$timer, &$tracker){
                //delete the activities first
                $timer->timerActivities->each(function($activity) use (&$tracker){
                    if($activity->delete()){
                        $tracker++;
                    }
                });
                //delete the timer itself
                if($timer->delete()){
                    $tracker++;
                }
            });

            if($tracker == $toDelete){
                return Response::json(array('status' => 'success'));
            }else{
                return Response::json(array('status' => 'failure'));
            }

        }else{
            App::abort('500', 'Invalid timer');
        }
    }

    /**
     * Creates a new timer
     *
     * @author Khaled Ahmed
     */
    public function add()
    {
        $name = Input::get('name');

        $timer          = new Timer;
        $timer->user_id = $this->user->id;
        $timer->name    = $name;
        $timer->state   = 0;
        $timer->save();

        $timer->load('timerActivities');

        if($timer->id){
            return Response::json($timer);
        }
    }


}
