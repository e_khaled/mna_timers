Sablier.controller('statusCtrl', function($scope, Auth){

  $scope.user = null;

  $scope.logout = function(ev){
    Auth.logout();
  };

  $scope.$on('user:loggedOut', function(){
    $scope.user = null;
  });

  $scope.$on('user:authenticated', function(ev, user){
    $scope.user = user;
  });



});