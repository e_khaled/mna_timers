Sablier.controller('loginCtrl', function($scope, Auth, $location, $http){

  $scope.creds = {}

  $scope.login = function(ev){
    if(ev){
      var el = angular.element(ev.target),
      button = el.find('button[type=submit]'),
      curHtml = button.html(),
      panel = el.parents(".panel");

      button.html('<i class="fa fa-spinner fa-spin"></i> Logging in...').attr('disabled', true);

      $http.post(BASEURL + '/login', $scope.creds)
      .success(function(user){
        if(user){
          Auth.login(user);
          $location.path('/timers');
        }else{
          el[0].reset();
          panel.addClass("shake");
          setTimeout(function(){
            panel.removeClass("shake");
          }, 1000);
        }
      })
      .error(function(){
        el[0].reset();
        panel.addClass("shake");
        setTimeout(function(){
          panel.removeClass("shake");
        }, 1000);
      })
      .finally(function(){
        button.html(curHtml).attr('disabled', false);
      });

    }
  };

  (function(){
    //if the user is already logged in
    //redirect them to the timers page
    Auth.check().then(function(){
      $location.path('/timers');
    });
  })();

});