<?php

class BaseController extends Controller {

    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */
    protected function setupLayout()
    {
        if ( ! is_null($this->layout))
        {
            $this->layout = View::make($this->layout);
        }
    }

    /**
     * CatchAll method to handle missing controller methods
     *
     * @return void
     * @author Khaled Ahmed
     */
    public function missingMethod($parameters = array())
    {
        return '
            <h1>404 Not Found</h1>
            <p>Page Not Found</p>
            <p><small>Err: MMeth01</small></p>
        ';
    }

}
