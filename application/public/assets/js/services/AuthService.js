/*
* Provides a clean way to handle authentication logic
*
* Broadcasts events regarding the logged in state of the user
* which can then be listened to by other controllers
*/

Sablier.factory('Auth', function($q, $rootScope, $location, $http){
  var user = null;
  return {
    login: function(user){
      user = user;
      $rootScope.$broadcast('user:authenticated', user);
    },
    logout: function(){
      user = null;
      $http.get(BASEURL + '/logout')
      .success(function(){
        user = null;
        $rootScope.$broadcast('user:loggedOut');
        $location.path('/login');
      })
      .error(function(){});
    },
    user: function(){
      return user;
    },
    loggedIn: function(){
      return !(user == null);
    },
    check: function(force){
      //returns a promise which is resolved or rejected
      //depending on whether the user is logged in or not
      var deferred = $q.defer();

      if(!this.loggedIn() || force){
        $http.get(BASEURL + '/userProfile')
        .success(function(resp){
          if(resp){
            user = resp;
            $rootScope.$broadcast('user:authenticated', user);
            deferred.resolve(true);
          }else{
            deferred.reject();
            $rootScope.$broadcast('user:loggedOut');
          }
        })
        .error(function(){
          deferred.reject();
          $rootScope.$broadcast('user:loggedOut');
        });
      }else{
        deferred.resolve(true);
      }

      return deferred.promise;
    }
  }
});