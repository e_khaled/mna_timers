#!/bin/bash
OVERRIDE_NONE="AllowOverride None"
OVERRIDE_ALL="AllowOverride All"

/bin/sed -i "s/AllowOverride None/AllowOverride All/g" /etc/httpd/conf.d/10-default_vhost_80.conf

service httpd restart