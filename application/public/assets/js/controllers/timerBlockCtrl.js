Sablier.controller('timerBlockCtrl', function($scope, $http){

  var aggregate = 0;

  $scope.id = $scope.timer.id;
  $scope.name = $scope.timer.name;
  $scope.identifier = $scope.timer.identifier;
  $scope.state = $scope.timer.state;
  $scope.timer_activities = $scope.timer.timer_activities;
  $scope.total_time = '00:00:00';
  $scope.loading = 1;

  $scope.$on('timer:tick', function(){
    if($scope.state == 1){
      aggregate += 1000;
      $scope.total_time = readableTime(aggregate);
    }
  });

  $scope.stop = function(ev){
    if($scope.state == 1){
      var current = aggregate,
      ts = (new Date).getTime();
      $scope.loading = 1;
      $http.post(BASEURL + '/timer/stop',{
        ts: ts,
        id: $scope.id
      })
      .success(function(dt){
        if(dt && dt.status && dt.status == 'success'){
          if(dt.state == 0){
            $scope.state = 0;
            aggregate = 0;
            var activity = $scope.timer_activities[$scope.timer_activities.length - 1];
            if(activity){
              activity.stopped_at = dt.stopped_at;
            }
            aggregateTime();
          }else{
            $scope.state = 1;
          }
        }else{
          $scope.state = 1;
        }
      })
      .error(function(){
        $scope.state = 1;
      })
      .finally(function(){
        $scope.loading = 0;
      })
    }
  };

  $scope.start = function(ev){
    if($scope.state == 0){
      $scope.loading = 1;
      $http.post(BASEURL + '/timer/start',{id: $scope.id})
      .success(function(dt){
        if(dt && dt.status && dt.status == 'success'){
          if(dt.state == 1){
            $scope.timer_activities.push(dt.timer_activity);
            aggregate = 0;
            aggregateTime();
            $scope.state = 1;
          }else{
            $scope.state = 0;
          }
        }else{
          $scope.state = 0;
        }
      })
      .error(function(){
        $scope.state = 0;
      })
      .finally(function(){
        $scope.loading = 0;
      });
    }
  };

  $scope.remove = function(idx){
    if(confirm("Are you sure?")){
      $scope.loading = 1;
      $http.post(BASEURL + '/timer/remove',{id: $scope.id})
      .success(function(dt){
        if(dt && dt.status && dt.status == 'success'){
          //emit an even that is captured by the
          //parent controller and the timer is removed
          $scope.$emit('timer:removed', idx);
        }
      })
      .finally(function(){
        $scope.loading = 0;
      });
    }
  };

  (function(){
    aggregateTime();
  })();

  function aggregateTime(){

    //calculate the elapsed time so far
    //by aggregating the time differences in all available timer_activity entries

    var keepLooping = true;

    angular.forEach($scope.timer_activities, function(act, i){
      if(keepLooping){
        var start = new Date(act.created_at).getTime(),
        end = new Date();
        end = end.getTime() + (end.getTimezoneOffset() * 60 * 1000);
        if(act.stopped_at){
          end = new Date(act.stopped_at).getTime();
        }
        var diff = end - start;
        aggregate += diff;
        if(!act.stopped_at){
          //if you hit a row with no stopped_at, no need to loop anymore
          keepLooping = false;
        }
      }
    });
    $scope.total_time = readableTime(aggregate);
    $scope.loading = 0;
  }


});

function readableTime(time){

  //given a timestamp, returns a Human readable string

  var secs = time / 1000,
  hours = Math.floor(secs / (60 * 60)),
  div_m = secs % (60 * 60),
  minutes = Math.floor(div_m / 60),
  div_s = div_m % 60,
  seconds = Math.ceil(div_s);

  var arr = [
    ((hours < 10) ? '0'+ hours : hours),
    ((minutes < 10) ? '0'+ minutes : minutes),
    ((seconds < 10) ? '0'+ seconds : seconds)
    ];
  return arr.join(':');
}