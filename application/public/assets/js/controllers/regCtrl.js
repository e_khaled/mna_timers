Sablier.controller('regCtrl', function($scope, $http, Auth, $location){

  $scope.user = {}

  $scope.register = function(ev){
    if(ev){
      var el = angular.element(ev.target),
      button = el.find('button[type=submit]'),
      curHtml = button.html();

      button.html('<i class="fa fa-spinner fa-spin"></i> Registering...').attr('disabled', true);

      $http.post(BASEURL + '/register', $scope.user)
      .success(function(user){
        if(user){
          Auth.login(user);
          $location.path('/timers');
        }else{
          $scope.user = {}
          alert("Registration could not be completed due to an internal error");
        }
      })
      .error(function(resp, code){
        if(resp && resp.email){
          el.find('input[name=username]').focus();
          alert(resp.email[0]);
        }else{
          $scope.user = {}
          alert("Registration could not be completed due to an internal error");
        }
      })
      .finally(function(){
        button.html(curHtml).attr('disabled', false);
      });

    }
  };

  (function(){
    //if the user is already logged in
    //redirect them to the timers page
    Auth.check().then(function(){
      $location.path('/timers');
    });
  })();


});